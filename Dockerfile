FROM golang:1.14

WORKDIR /go/src/app
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...
RUN go build -v

EXPOSE 3210

CMD ["ecowitt", "-username", "foo", "-password","bar","-server","tcp://10.0.0.1:1883"]
