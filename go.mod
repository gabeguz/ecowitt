module gitlab.com/gabeguz/ecowitt

go 1.13

require (
	github.com/eclipse/paho.mqtt.golang v1.3.2
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
)
