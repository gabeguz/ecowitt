# ecowitt

## UPDATE

There is now an official Ecowitt Home Assistant integration:
https://www.home-assistant.io/integrations/ecowitt/ which works really well.
You should probably use that instead.

A program to receive updates from the ecowitt GW1000 wireless gateway, and
format them to send along to Home Assistant via MQTT.


## The problem

I needed some cheap temperature sensors to track the temperature in my chicken
coop over the winter.  I wanted something that didn't rely on a hosted cloud
solution but that I could use stand alone at my own house.   The ecowitt weather
station was inexpensive and could handle up to 8 remote temperature sensors.
You could also configure it to send data to a server of your chosing.  This made
it a good enough choice to try out.  Ideally I'd have found something that was
open source, but I wasn't able to.  Once I had the temperature data, I wanted to
be able to take action based on the current temperature, for example to send
myself an alert if it went too low in the chicken coop.   As I was already using
Home Assistant for home automation, it made sense to try and get the data
there.

After researching sensors on Home Assistant, and how they work, it seemed like
the easiest and most flexible approach would be to create MQTT messages that I
could send to a listening MQTT server that Home Assistant would read from.  

## The solution

```plantuml
GW100 -> ecowitt: POST sensor data
ecowitt --> GW100: 200 OK
ecowitt -> "MQTT Broker": Configuration Messsage
ecowitt -> "MQTT Broker": State Updates
"MQTT Broker" -> "Home Assistant": Configuration Message
"MQTT Broker" -> "Home Assistant": State Updates
```

The GW1000 collects the data from connected sensors and sends it to a server
that you specify.  It sends it as a POST request over HTTP.  This program
(currently named ecowitt, but I'll probaby rename this as it seems to generic)
listens for a connection from the weather station, interprets it, and reformats
it into MQTT messages that Home Assistant can understand.

## Prior Art

I found a pre-existing solution to this problem:
https://github.com/iz0qwm/ecowitt_http_gateway 

But it did not quite fit my use-case since it doesn't directly put information
into Home Assistant.  

## Installation

### Prerequisites 

- Home Assistant
- An MQTT broker: I use mosquitto, but any MQTT broker should work equally well.

### Precompiled Binaries

- Download the `ecowitt` binary for your system
  [here](https://gitlab.com/gabeguz/ecowitt/-/releases)


### Build from source

In order to build the `ecowitt` binary from source, you will need the [Go
Programming Language](https://golang.org/dl/) installed on your system.  (You
can check if it's already installed by typing: `go version` on your system.

Once you have Go installed, you can download, build, and install the ecowitt
binary using the `go get` command:

```
$ go get gitlab.com/gabeguz/ecowitt
```

### Setup

- Configure Home Assistant to consume MQTT messages from your MQTT broker.
- Using command line arguments to this binary, configure it to send messages to
  the MQTT broker: `ecowitt -server tcp://<IP ADDRESS OF MQTT SERVER>:<PORT>
  -retained true`
  - You may also add in the username and password configured in HASS or in your
	MQTT broker directly such as `ecowitt -username mqtt -password mypassword
	-server tcp://<IP ADDRESS OF MQTT SERVER>:1883`
- Configure your ecowitt GW1000 to send requests to this binary.
  - Using the WSView application, configure the GW1000 to send data to this binary (ecowitt). In the WSView app, navigate to the 'Weather Services' page: ![weather-services-small](https://gitlab.com/gabeguz/ecowitt/-/wikis/uploads/13581571137efef1895cf873b0c86f18/weather-services-small.png)
  
  - From there, click/press through the 'Next' buttons until you reach the 'Customized' page: 
    
    ![station-config-small](https://gitlab.com/gabeguz/ecowitt/-/wikis/uploads/ecbbddf80e2c2bfb88d7b83cb6527321/station-config-small.png)

  - Leave the Protocol Type as "Ecowitt"
  - Set the Server IP of the IP of the host running the ecowitt binary.
  - Leave the Path as "/"
  - Set the port to 3210
  - The default interval of 60 can be used or you can customize it.
